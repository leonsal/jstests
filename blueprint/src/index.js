import * as React from "react";
import * as ReactDOM from "react-dom";
const e = React.createElement;
import {
    Alignment,
    AnchorButton,
    Button,
    ButtonGroup,
    Classes,
    Intent,
    Menu,
    MenuDivider,
    MenuItem,
    Navbar,
    NavbarHeading,
    NavbarDivider,
    NavbarGroup,
    Popover,
    Position,
    Spinner,
    Tabs,
    Tab
} from "@blueprintjs/core";

function start() {
    const root = document.getElementById("root");

    const menu1 = e(Menu, {className:Classes.ELEVATION_1},
        e(MenuItem, {icon:"graph", text:"Graph"}),
        e(MenuItem, {icon:"map", text:"Map"}),
        e(MenuItem, {icon:"th", text:"Table", shouldDismissPopover:false}),
        e(MenuItem, {icon:"zoom-to-fit", text:"Nucleus", disabled:true}),
        e(MenuDivider),
        e(MenuItem, {icon:"cog", text:"Settings..."},
            e(MenuItem, {icon:"add", text:"Add new application", disabled:true}),
            e(MenuItem, {icon:"remove", text:"Remove application"})
        )
    );
    const popover1 = e(Popover, {minimal:true, content:menu1, position:Position.BOTTOM_LEFT},
        e(Button, {icon:"share", minimal:true, text:"Open in..."})
    );


    const panel1 = e('div',null,
        e('h3', null, "Example panel1"),
        e('p', {className: Classes.RUNNING_TEXT}, `
            HTML is great for declaring static documents, but it falters when we try to use it for declaring dynamic
            views in web-applications. AngularJS lets you extend HTML vocabulary for your application. The resulting
            environment is extraordinarily expressive, readable, and quick to develop.`
        )
    );

    ReactDOM.render(e('div', null, 
        // Button --------------------------------------------------------------
        e(Button, { icon: "refresh", intent: Intent.NONE, text: "primary" }),
        e(Button, { rightIcon: "refresh", intent: Intent.SUCCESS }, "success"),
        e(Button, { intent: Intent.WARNING }, "warning"),
        e(Button, { intent: Intent.DANGER }, "danger"),
        e("hr", null),
        e(Spinner, { className: Classes.SMALL, intent: Intent.PRIMARY}),
        // Button group -------------------------------------------------------
        e("hr", null),
        e(ButtonGroup, {minimal: true},
            e(Button, {icon:"database"}, "Queries"),
            e(Button, {icon:"function"}, "Functions"),
            e(AnchorButton, {rightIcon:"caret-down"}, "Options")
        ),
        // Menu -------------------------------------------------------
        e("hr", null),
        popover1, popover1,
        // Navbar------------------------------------------------------
        e("hr", null),
        e(Navbar, null,
            e(NavbarGroup, {align:Alignment.LEFT},
                e(NavbarHeading, {}, "Blueprint"),
                e(NavbarDivider),
                e(Button, {className:"bp3-minimal", icon:"home", text:"Home"}),
                e(Button, {className:"bp3-minimal", icon:"document", text:"Files"}),
                popover1, popover1, popover1
            )
        ),
        // Tabs ------------------------------------------------------
        e("hr", null),
        e(Tabs, { id:"TabsExample", key:"horizontal", renderActiveTabPanelOnly:false, vertical:false},
            e(Tab, {id:"rx", title:"React", panel:panel1}),
            e(Tab, {id:"ng", title:"Angular", panel:panel1}),
            e(Tab, {id:"mb", title:"Ember", panel:panel1, panelClassName:"ember-panel"}),
            e(Tab, {id:"bb", disabled:true, title:"Backbone", panel:panel1})
        )
    ), root);
}
start();
 


