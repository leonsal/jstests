import * as React from "react";
const e = React.createElement;

import {Menubar} from "primereact/menubar";
import {InputText} from "primereact/inputtext";

export class AppMenubar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [
               {
                    label:'File',
                    //icon:'pi pi-fw pi-file',
                    items:[
                       {
                          label:'New',
                          icon:'pi pi-fw pi-plus',
                          items:[
                             {
                                label:'Bookmark',
                                icon:'pi pi-fw pi-bookmark',
                                command: ()=>{console.log('Bookmark clicked');}
                             },
                             {
                                 label:'Video',
                                 icon:'pi pi-fw pi-video',
                                 command: ()=>{console.log('Video clicked');}
                             },
 
                          ]
                       },
                       {
                          label:'Delete',
                          icon:'pi pi-fw pi-trash',
                          command: ()=>{console.log('Delete clicked');}
                       },
                       {
                          separator:true
                       },
                       {
                          label:'Export',
                          icon:'pi pi-fw pi-external-link',
                          command: ()=>{console.log('Export clicked');}
                       }
                    ]
               },
               { 
                  label:'Edit',
                  //icon:'pi pi-fw pi-pencil',
                  items:[
                     {
                        label:'Left',
                        icon:'pi pi-fw pi-align-left'
                     },
                     {
                        label:'Right',
                        icon:'pi pi-fw pi-align-right'
                     },
                     {
                        label:'Center',
                        icon:'pi pi-fw pi-align-center'
                     },
                     {
                        label:'Justify',
                        icon:'pi pi-fw pi-align-justify'
                     },

                  ]
               },
            ]
        };
    }

    render() {
        return e(Menubar, {model:this.state.items});
    }

}

