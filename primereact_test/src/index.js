import * as React from "react";
import * as ReactDOM from "react-dom";
const e = React.createElement;
import {Button} from "primereact/button";

import {AppMenubar} from "./menubar";

class ButtonTest extends React.Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        console.log("button clicked");
    }

    render() {
        return e('div', {style: {display:"flex", justifyContent: "space-evenly"}},
            e(Button, {label: "click", tooltip: "click to click", onClick: this.handleClick}),
            e(Button, {label: "click2", icon: "pi pi-check"}),
            e(Button, {label: "click3", icon: "pi pi-check", iconPos:"right"}),
            e(Button, {icon: "pi pi-check"}),
            e(Button, {label: "click4", icon: "pi pi-check", disabled:true})
        );
    }
}

function start() {
    const root = document.getElementById("root");

    ReactDOM.render(e('div', null, 
        e(ButtonTest),
        e(AppMenubar)
    ), root);
}
start();

