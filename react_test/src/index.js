import React from "react";
import ReactDOM from "react-dom";
import {Component1} from "./component";
import {Component2} from "./component";
import {Toggle} from "./component";
import {Tuner} from "./tuner";
const e = React.createElement;

function start() {
    const root = document.getElementById("root");
    ReactDOM.render(e('div', null, 
        e(Tuner, {ndigits:9, value:12345, sufix: " Hz", onChange: (val) => {console.log("Tuner1:", val);}}),
        e(Tuner, {ndigits:8, value:4321}),
        e(Tuner, {ndigits:7, value:2468}),
        e(Tuner, {ndigits:6, value:33}),
        e(Tuner, {ndigits:5, value:222}),
        e(Tuner, {ndigits:4, value:9999, groupsep: ','}),
        e(Tuner, {ndigits:3, value:99})
    ), root);
}
start();

