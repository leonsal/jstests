import React from "react";
import h from "react-hyperscript";
const e = React.createElement;

export class Component1 extends React.Component {
    render() {
        return e('div', null, 'hello react');
    }
}

export class Component2 extends React.Component {
    render() {
        return e('div', null, 
            e('h1', {className: 'hclass'}, 'header'),
            e('h2', null, "header2"),
            h('h3.hclass', null, "header3")
        );
    }
}

export class Component3 extends React.Component {
    render() {
        return h('div.example', [
            h('h1', 'header'),
            h('h2', "subheader")
        ]);
    }
}

export class Toggle extends React.Component {

    constructor(props) {
        super(props);
        this.state = {isToggleOn: true};
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState(state => ({
            isToggleOn: !state.isToggleOn
        }));
    }

    render() {
        return e('button', {onClick:this.handleClick}, this.state.isToggleOn ? "ON":"OFF");
    }
}
