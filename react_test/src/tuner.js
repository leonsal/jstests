import React from "react";
const e = React.createElement;


export class Tuner extends React.Component {

    /**
     * @param {props} props 
     * props.ndigits    - number of digits
     * props.value      - initial valueA
     * props.sep        - group separator char
     * props.sufix      - sufix strings
     * props.onChange
     */
    constructor(props) {

        super(props);
        // Component state
        this.state = {
            digits: new Array(props.ndigits)
        };
        // Builds array to digits components references
        this._digiRefs = [];
        for (let i = 0; i < props.ndigits; i++) {
            this._digiRefs.push(React.createRef());
        }

        this.handleWheel = this.handleWheel.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this._total = this.props.ndigits + Math.floor((this.props.ndigits-1)/3);
        this._min = 0;
        this._max = Math.pow(10, this.props.ndigits)-1;
        this._groupsep = props.groupsep === undefined ? '.' : props.groupsep;
    }

    /**
     * Returns the current value of the component
     */
    getValue() {

        let value = 0;
        for (let i = 0 ; i < this.props.ndigits; i++) {
            const pten = Math.pow(10, this.props.ndigits-i-1);
            value += this.state.digits[i] * pten;
        }
        return value;
    }

    setValue(val) {

        if (val == this.getValue()) {
            return;
        }
        if (val < this._min || val > this._max) {
            return;
        }
        this.setState((state, props) => {
            const digits = new Array(this.props.ndigits);
            for (let i = 0 ; i < this.props.ndigits; i++) {
                const pten = Math.pow(10, this.props.ndigits-i-1);
                digits[i] = Math.floor(val / pten);
                val = val % pten;
            }
            return {digits: digits};
        });
    }

    incDigit(index) {
        const pten = Math.pow(10, this.props.ndigits-index-1);
        this.setValue(this.getValue() + pten);
    }

    decDigit(index) {
        const pten = Math.pow(10, this.props.ndigits-index-1);
        this.setValue(this.getValue() - pten);
    }

    focusNextDigit(index) {

        if (index >= this.props.ndigits - 1) {
            return;
        }
        index++;
        this._digiRefs[index].current.focus();
    }

    focusPrevDigit(index) {

        if (index == 0) {
            return;
        }
        index--;
        this._digiRefs[index].current.focus();
    }

    setDigit(index, digval) {

        let value = 0;
        const digits = this.state.digits.slice();
        digits[index] = digval;
        for (let i = 0; i < digits.length; i++) {
            const pten = Math.pow(10, this.props.ndigits - i - 1);
            value += digits[i] * pten;
        }
        this.setValue(value);
    }

    handleWheel(index, event) {
        if (event.deltaY > 0) {
            this.incDigit(index);
        } else {
            this.decDigit(index);
        }
    }

    handleKeyDown(index, event) {

        if (event.key >= "0" && event.key <= "9") {
            const dv = parseInt(event.key);
            this.setDigit(index, dv);
            this.focusNextDigit(index);
            return;
        }
        switch (event.key) {
            case "ArrowUp":
                this.incDigit(index);
                break;
            case "ArrowDown":
                this.decDigit(index);
                break;
            case "ArrowRight":
                this.focusNextDigit(index);
                break; case "ArrowLeft":
                this.focusPrevDigit(index);
                break;
            case "Backspace":
                this.setDigit(index, 0);
                this.focusPrevDigit(index);
                break;
        }
    }

    componentDidMount() {

        if (this.props.value !== undefined) {
            this.setValue(this.props.value);
        } else {
            this.setValue(0);
        }
    }

    componentDidUpdate() {

        if (this.props.onChange) {
            this.props.onChange(this.getValue());
        }
    }


    createDigit(pos) {

        // Create and returns group separator
        if ((this._total - pos) % 4 == 0) {
            return e('span', {key: pos}, this._groupsep);
        }
        // Calculates index of digit in array
        const npoints = Math.floor((this.props.ndigits-1)/3);
        let index = pos - (npoints-Math.floor((this._total-pos)/4));
        // Create and returns digit
        return e('span', {
                key:        pos,
                ref:        this._digiRefs[index],
                tabIndex:   index,
                onWheel:    this.handleWheel.bind(this, index),
                onKeyDown:  this.handleKeyDown.bind(this, index),
            }, this.state.digits[index]);
    }

    render() {
        const children = [];
        let pos = 0;
        for (; pos < this._total; pos++) {
            children.push(this.createDigit(pos));
        }
        if (this.props.sufix !== undefined) {
            children.push(e('span', {key: pos}, this.props.sufix));
        }
        return e('div', {style: {fontSize:"40px"}}, children);
    }
}
