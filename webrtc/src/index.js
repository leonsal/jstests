// Used by side-effects (install shims)
import adapter from "webrtc-adapter";


function handleMedia(stream) {


    console.log("Media:", stream);

}

function handleError(err) {

    console.log("Error:", err);
}

function main() {

    const videoEl = document.querySelector("video");
    navigator.getUserMedia({audio: true, video: false}, handleMedia, handleError);
}

main();


